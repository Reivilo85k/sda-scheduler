import org.junit.Test;
import static  org.junit.Assert.*;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.NoSuchElementException;


public class PersonTest {

    @Test
    public void testInputDay() {
        int day = 0;

        do{
            day = 6;
        }while (day < 1 || day > 12);

        assert(day == 6);
    }

    @Test
    public void testInputMonth() {
        int month = 0;

        do{
            month = 6;
        }while (month < 1 || month > 12);

        assert(month == 6);
    }

    @Test
    public void testInputYear() {
        int year = 0;

        do{
            year = 6;
        }while (year < 1 || year > 12);

        assert(year == 6);
    }

    @Test
    public void testCreateDateOfBirth() {
        LocalDate helper1 = LocalDate.of(1000,1,1);
        LocalDate dateOfBirth = LocalDate.of(1000,1,1);
        LocalDate result = LocalDate.of(1986,9,25);

        do{
            try{
                int day = 25;
                int month = 9;
                int year = 1986;
                dateOfBirth = LocalDate.of(year, month, day);
            }catch (DateTimeException d){
                System.out.println("This is not a valid date");
            }
        }while (dateOfBirth.equals(helper1));

        assert(dateOfBirth.equals(result));

    }

    @Test
    public void testCreateDateOfBirthDateTimeExeception() {
        LocalDate helper1 = LocalDate.of(1000,1,1);
        LocalDate dateOfBirth = LocalDate.of(1000,1,1);
        LocalDate result = LocalDate.of(1986,9,25);

        try{
            int day = 30;
            int month = 2;
            int year = 1986;
            dateOfBirth = LocalDate.of(year, month, day);
            System.out.println("Test failed");
        }catch (DateTimeException d){
            System.out.println("test validated");
        }
    }

    @Test
    public void testIsHasJavaKnowledgeWhenTrueAndIsAuthorized() {
        String choice = "";
        boolean result = false;

        do {
            try {
                choice = "y";
            } catch (NoSuchElementException e) {
                System.out.println("Incorrect Input");
            }
        } while (!choice.equalsIgnoreCase("Y") && !choice.equalsIgnoreCase("N"));

        if (choice.equalsIgnoreCase("y")) {
            result = true;
        } else {
            result = false;
        }

        assertTrue(result);
    }

    @Test
    public void testIsHasJavaKnowledgeWhenFalse() {
        String choice = "";
        boolean result = false;

        do {
            try {
                choice = "n";
            } catch (NoSuchElementException e) {
                System.out.println("Incorrect Input");
            }
        } while (!choice.equalsIgnoreCase("Y") && !choice.equalsIgnoreCase("N"));

        if (choice.equalsIgnoreCase("y")) {
            result = true;
        } else {
            result = false;
        }

        assertFalse(result);
    }

    @Test
    public void testCreatePerson() {
        LocalDate helper= LocalDate.of(1981,5,31);
        Student expected = new Student("John", "Dupont", helper, true);

        String firstName = "John";
        String lastName = "Dupont";
        LocalDate dateOfBirth = LocalDate.of(1981,5,31);
        boolean hasJavaKnowledgeOrIsAuthorized = true;

        Student result = new Student(firstName, lastName, dateOfBirth,hasJavaKnowledgeOrIsAuthorized);

        assert(result.getFirstName().equals(expected.getFirstName()));
        assert(result.getLastName().equals(expected.getLastName()));
        assert(result.getDateOfBirth().equals(expected.getDateOfBirth()));
        assert(result.isHasPreviousJavaKnowledge() == expected.isHasPreviousJavaKnowledge());
    }
    @Test
    public void testCalculateNumberOfStudentsWithJavaKnowledge(){
        int expected = 2;
        int result;

        Student student1 = new Student("straw", "Abe", LocalDate.of(1986, 5, 12), false);
        Student student2 = new Student("straw", "Babe", LocalDate.of(1986, 5, 12), true);
        Student student3 = new Student("straw", "Cabe", LocalDate.of(2016, 5, 12), false);
        Student student4 = new Student("straw", "Dabe", LocalDate.of(1993, 5, 12), true);
        Student student5 = new Student("straw", "Ebe", LocalDate.of(1950, 5, 4), false);

        Trainer trainer1 = new Trainer("straw", "man0", LocalDate.of(1986, 5, 25), true);

        Group group1 = new Group(trainer1, student1, student2, student3, student4, student5, "Group 1");

        result = group1.calculateNumberOfStudentsWithNoJavaKnowledge(group1.getGroupStudentsList());

        assertEquals(expected, result);
    }
}
