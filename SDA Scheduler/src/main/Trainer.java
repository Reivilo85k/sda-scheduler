import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Trainer extends Person {
    private boolean isAuthorized;
    Scanner input = new Scanner(System.in);

    public Trainer(String firstName, String lastName, LocalDate dateOfBirth, boolean isAuthorized) {
        super(firstName, lastName, dateOfBirth);
        this.isAuthorized = isAuthorized;
    }

    public Trainer() {
    }

    public boolean isAuthorized() {
        return isAuthorized;
    }

    public void setAuthorized(boolean authorized) {
        isAuthorized = authorized;
    }

    @Override
    public String toString() {
        return "\nTrainer :" +
                "\nFirst Name " + firstName +
                "\nLast Name " + lastName +
                "\nBorn " + dateOfBirth +
                "\nisAuthorized=" + isAuthorized
                + "\n";
    }
    @Override
    public Trainer createPerson() {
        System.out.println("Trainer Input");

        String firstName = createFirstName();
        String lastName = createLastName();
        LocalDate dateOfBirth = createDateOfBirth();
        boolean hasJavaKnowledge = isLicensed();

        return new Trainer(firstName, lastName, dateOfBirth,hasJavaKnowledge);
    }

    @Override
    public String createFirstName(){
        System.out.println("Please insert new trainer first name");
        return input.nextLine();
    }

    @Override
    public String createLastName() {
        System.out.println("Please insert new trainer last name");
        return input.nextLine();
    }

    @Override
    public LocalDate createDateOfBirth(){
        LocalDate helper = LocalDate.of(1000,1,1);
        LocalDate dateOfBirth = LocalDate.of(1000,1,1);

        do{
            try{
                System.out.println("Please insert new trainer date of birth");
                int day = inputDay();
                int month = inputMonth();
                int year = inputYear();
                dateOfBirth = LocalDate.of(year, month, day);
            }catch (DateTimeException d){
                System.out.println("This is not a valid date");
            }
            //Using LocalDate permits to catch an exception if a non-existing date is input
            //As long as there is no valid input the date of birth will be equal to the helper and it will ask for a new input
        }while (dateOfBirth.equals(helper));

        return dateOfBirth;
    }

    @Override
    public Integer inputDay(){
        int day = 0;

        do{
            try {
                Scanner in = new Scanner(System.in);
                System.out.println("Select a day (1-31)");
                day = in.nextInt();
            }catch (InputMismatchException i) {
                System.out.println("Incorrect Input");
            }
            //it will loop until an integer between 1 and 31 is chosen.
            // If the user later chose a month where there is no 31st, it will be caught in createDateOfBirth method
            //and this method will be recalled
        }while (day < 1 || day > 31);

        return  day;
    }

    @Override
    public Integer inputMonth(){
        int month = 0;

        do{
            try {
                Scanner in = new Scanner(System.in);
                System.out.println("Select a month (1-12)");
                month = in.nextInt();
            }catch (InputMismatchException  i){
                System.out.println("Incorrect Input");
            }
            //it will loop until an integer between 1 and 12 is chosen.
        }while (month < 1 || month > 12);

        return month;
    }

    @Override
    public Integer inputYear(){
        int year = 0;

        do{
            try {
                Scanner in = new Scanner(System.in);
                System.out.println("Select a year");
                year = in.nextInt();
            }catch (InputMismatchException  i){
                System.out.println("Incorrect Input");
            }
            //it will loop until an acceptable choice is input
        }while (year < 1850 || year > 2020);

        return year;
    }

    public boolean isLicensed() {
        String choice = "";
        Scanner in = new Scanner(System.in);

        do {
            try {
                System.out.println("is the trainer licensed Y/N?");
                choice = in.nextLine();
            } catch (NoSuchElementException e) {
                System.out.println("Incorrect Input");
            }
            //loops until y or n is input, with in lower or upper case
        } while (!choice.equalsIgnoreCase("Y") && !choice.equalsIgnoreCase("N"));

        if (choice.equalsIgnoreCase("y")) {
            return true;
        } else {
            return false;
        }
    }
}
