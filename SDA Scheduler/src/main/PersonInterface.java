import java.time.LocalDate;

public interface PersonInterface {
   Person createPerson();
   String createFirstName();
   String createLastName();
   LocalDate createDateOfBirth();
   Integer inputDay();
   Integer inputMonth();
   Integer inputYear();
}
