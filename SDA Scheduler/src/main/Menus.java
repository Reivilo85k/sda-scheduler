import java.util.InputMismatchException;
import java.util.Scanner;

public class Menus {

    //Menu 1 is dedicated to create students, trainers, groups and lists populating the school
    public static School Menu1(Scanner input){
        System.out.println("Welcome to SDA Scheduler");
        int choice = 0;
        School school = new School();

        do{
            try {
                System.out.println("\n\nPlease chose an option: \n" +
                        //added an automatic creation option to avoid inputing 15 students at every trial
                        "1. Create the groups automatically\n" +
                        "2. Create the groups manually");
                choice = input.nextInt();
            }catch (IllegalArgumentException e){
                System.out.println("Incorrect input. Chose 1 or 2");
            }
            //wrong syntax is caught and it will loop until a correct one is input
        }while (choice < 1 || choice > 2);

        switch (choice){
            case (1):
                school = Initialization.automaticInitialization();
                System.out.println("15 Students created\n" +
                        "Student List created\n" +
                        "3 Trainers created\n" +
                        "Trainers List created\n" +
                        "4 groups created\n" +
                        "Populating groups\n" +
                        "Group List created\n" +
                        "Automatic Initialization complete");
                break;
            case (2):
                school = Initialization.manualInitializing();
                break;
            default:
                System.out.println("Something went wrong, please restart the program");
        }
        return school;
    }

    public static void Menu2(School school){
        int choice = 0;
        int helper = 0;

        while(helper < 1) {
            do {

                try {
                    Scanner in = new Scanner (System.in);
                    System.out.println("\n\nPlease select an option : " +
                            "\n1. Display all the students alphabetically" +
                            "\n2 .display the group(s) with the most students" +
                            "\n3. Display all students under the age of 25" +
                            "\n4. Display all the students grouped by trainers" +
                            "\n5. Display all the students with previous java knowledge" +
                            "\n6. Display the group(s) with the highest number of students with no java knowledge" +
                            "\n7. Remove all the students younger than 20 from all groups" +
                            "\n8. Exit menu");
                    choice = in.nextInt();
                } catch (IllegalArgumentException | InputMismatchException f) {
                    System.out.println("Incorrect input. Chose a number between 1 and 6");
                }
            } while (choice < 1 || choice > 8);

            switch (choice) {
                case (1):
                    school.displayGroupStudentsAlphabetically(school.getStudentList());
                    break;
                case (2):
                    school.displayMaxStudentsGroup(school.getGroupList());
                    break;
                case (3):
                    school.displayUnder25Students(school.getStudentList());
                    break;
                case (4):
                    school.displayStudentsGroupedByTrainers(school.getGroupList());
                    break;
                case (5):
                    school.displayStudentsWithJavaKnowledge(school.getStudentList());
                    break;
                case (6):
                    school.displayGroupMaxNoJavaKnowledge(school.getGroupList());
                    break;
                case (7):
                    school.removeUnder20Students(school.getGroupList());
                    break;
                case (8):
                    helper++;
                    break;
                default:
                    System.out.println("Something went wrong, please restart the program");
            }
        }
    }
}
